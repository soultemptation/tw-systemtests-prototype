﻿namespace Travel.SystemTests.Prototype.Bootstrap
{
    public enum Browser
    {
        Firefox,
        Chrome,
        InternetExplorer,
        Opera,
        Safari
    }
}
﻿namespace Travel.SystemTests.Prototype.Bootstrap
{
    using System;

    using OpenQA.Selenium;
    using OpenQA.Selenium.IE;
    using OpenQA.Selenium.Support.UI;

    using Travel.SystemTests.Prototype.Testrunner;

    public class TravelDriver
    {
        private readonly ITestRunner testRunner;

        public string TargetUrl { get { return targetUrl; } }

        private readonly string targetUrl = "http://travel-test.travel.ch/de/";

        private IWebDriver driver;

        public TravelDriver(Browser browser, Version version, Platform platform)
        {
            testRunner = new SauceLabsTestRunner(browser, version, platform);
            //testRunner = new CrossBrowserTestRunner(browser, version, platform);
            //testRunner = new LocalTestRunner(browser, version, platform);
            driver = testRunner.GetDriver();
            SetLoginCookie();
        }

        public IWebDriver GetDriver()
        {
            return driver;
        }   

        private void SetLoginCookie()
        {
            // Navigate to the test to set the cookie. Else it won't work on all browsers (e.g. IE).
            driver.Navigate().GoToUrl(targetUrl);
            var cookie = new Cookie("NSC_SPA", "test", "/");
            driver.Manage().Cookies.AddCookie(cookie);
        }
    }
}
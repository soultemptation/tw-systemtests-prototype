﻿namespace Travel.SystemTests.Prototype
{
    using System;
    using System.Linq;
    using System.Text.RegularExpressions;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using NUnit.Framework;

    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.UI;

    using Travel.SystemTests.Prototype.Bootstrap;

    using Assert = NUnit.Framework.Assert;
    using Platform = Travel.SystemTests.Prototype.Bootstrap.Platform;
    using Version = Travel.SystemTests.Prototype.Bootstrap.Version;

    [TestFixture("Firefox", "25", "Windows7")]
    [TestFixture("InternetExplorer", "11", "Windows7")]
    [TestFixture("Chrome", "38", "Windows8")]
    [TestFixture("Safari", "7", "MacOSX10_9")]
    [DeploymentItem(@"./resources/chromedriver.exe", "resources")]
    public class SearchTests
    {
        private readonly TravelDriver travelDriver;

        private IWebDriver driver;

        private readonly string targetUrl;

        private Find find;

        private TravelAssert travelAssert;

        public SearchTests(string browser, string version, string platform)
        {
            var browserEnum = (Browser)Enum.Parse(typeof(Browser), browser);
            var versionEnum = (Version)Enum.Parse(typeof(Version), "_" + version);
            var platformEnum = (Platform)Enum.Parse(typeof(Platform), platform);
            travelDriver = new TravelDriver(browserEnum, versionEnum, platformEnum);
            targetUrl = travelDriver.TargetUrl;
        }

        [SetUp]
        public void SetUp()
        {
            driver = travelDriver.GetDriver();
            find = new Find(driver);
            travelAssert = new TravelAssert(driver, targetUrl);
        }

        [TearDown]
        public void TearDown()
        {
            driver.Quit();
        }

        [Test]
        public void AggregationOfSearchTests()
        {
            WhenEntering_ber_InTheSearchFieldResultsAppearInADropdown();
            //WhenPerformingAnEmptySearchItShouldGiveResults();
            //WhenEnteringZermattAndSearchWithSearchButtonItShouldGiveResults();
            //WhenEntering_Bernese_Ob_AndUseAutoSuggestionItShouldSelect_Bernese_Oberland_AndGiveResults();
            //WhenEntering_valais_AndHitEnterItShouldGiveResults();
        }

        public void WhenEntering_ber_InTheSearchFieldResultsAppearInADropdown()
        {
            // Run
            driver.Navigate().GoToUrl(targetUrl);

            var searchField = driver.FindElement(By.CssSelector(".js-filter-field"));
            searchField.SendKeys("ber");

            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            var suggestionItems = wait.Until(d =>
                {
                    var elements = d.FindElement(By.CssSelector(".ui-autocomplete")).FindElements(By.XPath(".//*"));
                    return elements.Any() ? elements : null ;
                });

            // Assert
            
            Assert.IsTrue(suggestionItems != null && suggestionItems.Any());
        }

        public void WhenPerformingAnEmptySearchItShouldGiveResults()
        {
            // Run
            driver.Navigate().GoToUrl(targetUrl);

            var submitButon = find.SearchButton();
            submitButon.Click();

            var lastBreadcumbItem = driver.FindElement(By.CssSelector(".modBreadcrumb h1"));

            // Assert
            var regex = new Regex(@"(\d+)");
            var match = regex.Match(lastBreadcumbItem.Text);
            Assert.IsTrue(match.Success, "WhenPerformingAnEmptySearchItShouldGiveResults");

            var accommodationCount = int.Parse(match.Value);
            Assert.IsTrue(
                accommodationCount >= 25000 && accommodationCount <= 35000, 
                string.Format(
                    "WhenPerformingAnEmptySearchItShouldGiveResults: " + 
                    "Expected to have between 25000 and 35000 accommodations, but got {0}", 
                    accommodationCount));
        }

        public void WhenEnteringZermattAndSearchWithSearchButtonItShouldGiveResults()
        {
            // Run
            driver.Navigate().GoToUrl(targetUrl);

            var inputField = driver.FindElement(By.Id("x:1"));
            inputField.SendKeys("Zermatt");

            find.SearchButton().Click();

            // Assert
            travelAssert.UrlWithNoParameters(
                "/switzerland/valais/zermatt/", 
                "WhenEnteringZermattAndSearchWithSearchButtonItShouldGiveResults");
            HasAtLeastOneSearchResult("WhenEnteringZermattAndSearchWithSearchButtonItShouldGiveResults");
        }

        public void WhenEntering_Bernese_Ob_AndUseAutoSuggestionItShouldSelect_Bernese_Oberland_AndGiveResults()
        {
            // Run
            driver.Navigate().GoToUrl(targetUrl);

            var inputField = driver.FindElement(By.Id("x:1"));
            inputField.SendKeys("Bernese Ob");

            var firstResult = driver.FindElement(By.CssSelector(".autocomplete .result"));
            firstResult.Click();

            // Assert
            Assert.AreEqual("Bernese Oberland",
                inputField.GetAttribute("value"),
                "WhenEntering_Bernese_Ob_AndUseAutoSuggestionItShouldSelect_Bernese_Oberland_AndGiveResults");

            // Run
            find.SearchButton().Click();

            // Assert
            travelAssert.UrlWithNoParameters(
                "/switzerland/bernese-oberland/",
                "WhenEnteringZermattAndSearchWithSearchButtonItShouldGiveResults");
            HasAtLeastOneSearchResult("WhenEntering_Bernese_Ob_AndUseAutoSuggestionItShouldSelect_Bernese_Oberland_AndGiveResults");
        }

        public void WhenEntering_valais_AndHitEnterItShouldGiveResults()
        {
            // Run
            driver.Navigate().GoToUrl(targetUrl);

            var inputField = driver.FindElement(By.Id("x:1"));
            inputField.SendKeys("valais");

            inputField.SendKeys(Keys.Enter);
            
            // Assert
            travelAssert.UrlWithNoParameters(
                "/switzerland/valais/",
                "WhenEntering_valais_AndHitEnterItShouldGiveResults");
            HasAtLeastOneSearchResult("WhenEntering_valais_AndHitEnterItShouldGiveResults");
        }

        private void HasAtLeastOneSearchResult(string message)
        {
            var serachResults = driver.FindElements(By.CssSelector(".ajax-content .c_objectlink"));
            Assert.GreaterOrEqual(serachResults.Count, 1, 
                string.Format("{0}: Expected to have one or more search results, but got {1}", 
                message, 
                serachResults.Count));
        }
    }
}

﻿namespace Travel.SystemTests.Prototype
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using OpenQA.Selenium;
    using OpenQA.Selenium.Chrome;
    using OpenQA.Selenium.Firefox;
    using OpenQA.Selenium.IE;

    [TestClass]
    [DeploymentItem(@"./resources/chromedriver.exe", "resources")]
    public class ExampleTests
    {

        [TestMethod]
        [Ignore]
        public void InternetExplorer()
        {
            var browser = new InternetExplorerDriver();
            browser.Navigate().GoToUrl("http://www.google.com/");
            browser.FindElement(By.Id("gbqfq")).SendKeys("Google");
            browser.FindElement(By.Id("gbqfq")).SendKeys(Keys.Enter);
            browser.Quit();
        }

        [TestMethod]
        [Ignore]
        public void Firefox()
        {
            var browser = new FirefoxDriver();
            browser.Navigate().GoToUrl("http://www.google.com/");
            browser.FindElement(By.Id("gbqfq")).SendKeys("Google");
            browser.FindElement(By.Id("gbqfq")).SendKeys(Keys.Enter);
            browser.Quit();
        }

        [TestMethod]
        [Ignore]
        public void Chrome()
        {
            var browser = new ChromeDriver("./resources");
            browser.Navigate().GoToUrl("http://www.google.com/");
            browser.FindElement(By.Id("gbqfq")).SendKeys("Google");
            browser.FindElement(By.Id("gbqfq")).SendKeys(Keys.Enter);
            browser.Quit();
        }
    }
}

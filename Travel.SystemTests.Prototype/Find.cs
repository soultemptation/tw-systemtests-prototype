﻿namespace Travel.SystemTests.Prototype
{
    using OpenQA.Selenium;

    public class Find
    {
        private readonly IWebDriver driver;

        public Find(IWebDriver driver)
        {
            this.driver = driver;
        }

        public IWebElement SearchButton()
        {
            return driver.FindElement(By.CssSelector(".main-search .submit"));
        }
    }
}
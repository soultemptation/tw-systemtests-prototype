﻿namespace Travel.SystemTests.Prototype
{
    using OpenQA.Selenium;

    public class TravelAssert
    {
        private readonly IWebDriver mDriver;

        private readonly string mBaseUrl;

        public TravelAssert(IWebDriver pDriver, string pBaseUrl)
        {
            mDriver = pDriver;
            mBaseUrl = pBaseUrl;
        } 

        public void UrlWithNoParameters(string pStringToSearch, string message)
        {
            var expectedUrl = mBaseUrl + "/" + pStringToSearch.Trim('/');
            var actualUrl = mDriver.Url.Trim('/');
            NUnit.Framework.Assert.AreEqual(
                expectedUrl,
                actualUrl,
                string.Format("{0}: Url [{1}] does not match [{2}]", 
                    message, actualUrl, expectedUrl));
        }
    }
}
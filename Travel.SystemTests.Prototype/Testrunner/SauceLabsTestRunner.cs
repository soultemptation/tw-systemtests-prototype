﻿namespace Travel.SystemTests.Prototype.Testrunner
{
    using System;
    using System.Text.RegularExpressions;

    using NUnit.Framework;

    using OpenQA.Selenium;
    using OpenQA.Selenium.Remote;

    using Travel.SystemTests.Prototype.Bootstrap;
    using Platform = Travel.SystemTests.Prototype.Bootstrap.Platform;
    using Version = Travel.SystemTests.Prototype.Bootstrap.Version;

    public class SauceLabsTestRunner : ITestRunner
    {
        private const string Username = "slang";

        private const string Password = "0a485ee5-7170-44b1-beba-cbf8ed22bbc4";

        private readonly Browser browser;

        private readonly Version version;

        private readonly Platform platform;

        public SauceLabsTestRunner(Browser browser, Version version, Platform platform)
        {
            this.browser = browser;
            this.version = version;
            this.platform = platform;
        }

        public IWebDriver GetDriver()
        {
            // construct the url to sauce labs
            var commandExecutorUri = new Uri("http://ondemand.saucelabs.com/wd/hub");

            DesiredCapabilities desiredCapabilities;

            // set up the desired capabilities
            var desiredCapabilites = GetDesirecCapabilities();

            // set the desired browser
            desiredCapabilites.SetCapability("platform", GetPlatform()); // operating system to use
            desiredCapabilites.SetCapability("username", Username); // supply sauce labs username
            desiredCapabilites.SetCapability("accessKey", Password); // supply sauce labs account key
            desiredCapabilites.SetCapability("name", TestName.Prettify(TestContext.CurrentContext.Test.Name, platform, browser, version)); // give the test a name

            // start a new remote web driver session on sauce labs
            var driver = new RemoteWebDriver(commandExecutorUri, desiredCapabilites);
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(30));
            driver.Manage().Timeouts().SetPageLoadTimeout(TimeSpan.FromSeconds(30));
            return driver;
        }

        private string GetPlatform()
        {
            switch (platform)
            {
                case Platform.Windows7:
                    return "Windows 7";
                case Platform.Windows8:
                    return "Windows 8";
                case Platform.MacOSX10_9:
                    return "OS X 10.9";
            }
            throw new Exception("Unknown plattform " + platform);
        }

        private DesiredCapabilities GetDesirecCapabilities()
        {
            DesiredCapabilities desiredCapabilities;
            switch (browser)
            {
                case Browser.Chrome:
                    desiredCapabilities = DesiredCapabilities.Chrome();
                    break;
                case Browser.Firefox:
                    desiredCapabilities = DesiredCapabilities.Firefox();
                    break;
                case Browser.InternetExplorer:
                    desiredCapabilities = DesiredCapabilities.InternetExplorer();
                    break;
                case Browser.Opera:
                    desiredCapabilities = DesiredCapabilities.Opera();
                    break;
                case Browser.Safari:
                    desiredCapabilities = DesiredCapabilities.Safari();
                    break;
                default:
                    throw new Exception("Unknown Browser " + browser);
            }
            return desiredCapabilities;
        }
    }
}
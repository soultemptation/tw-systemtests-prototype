﻿namespace Travel.SystemTests.Prototype.Testrunner
{
    using System.Text.RegularExpressions;

    using Travel.SystemTests.Prototype.Bootstrap;

    public class TestName
    {
        /// <summary>
        /// Takes the test name and puts a space between new words.
        /// e.g.:
        /// HelloWorld => Hello World
        /// dotNETRules => dot NET Rules
        /// </summary>
        /// <returns></returns>
        public static string Prettify(string testName, Platform platform, Browser browser, Version version)
        {
            testName = Regex.Replace(testName, "([A-Z])([A-Z])([a-z])|([a-z])([A-Z])", "$1$4 $2$3$5");
            var format = string.Format("{0} ({2} {3})", testName, platform, browser, version.ToString().Substring(1));
            return format;
        }
    }
}
namespace Travel.SystemTests.Prototype.Testrunner
{
    using OpenQA.Selenium;

    public interface ITestRunner
    {
        IWebDriver GetDriver();
    }
}
﻿namespace Travel.SystemTests.Prototype.Testrunner
{
    using System;
    using System.Text.RegularExpressions;

    using NUnit.Framework;

    using OpenQA.Selenium;
    using OpenQA.Selenium.Remote;

    using Travel.SystemTests.Prototype.Bootstrap;

    using Platform = Travel.SystemTests.Prototype.Bootstrap.Platform;
    using Version = Travel.SystemTests.Prototype.Bootstrap.Version;

    public class CrossBrowserTestRunner : ITestRunner
    {
        private const string Username = "travelwindow";

        private const string Password = "ub09e9a4dd29b094";

        private readonly Browser browser;

        private readonly Version version;

        private readonly Platform platform;

        public CrossBrowserTestRunner(Browser browser, Version version, Platform platform)
        {
            this.browser = browser;
            this.version = version;
            this.platform = platform;
        }

        public IWebDriver GetDriver()
        {
            // construct the url to sauce labs
            var commandExecutorUri = new Uri("http://hub.crossbrowsertesting.com:80/wd/hub");

            // set up the desired capabilities
            var desiredCapabilites = new DesiredCapabilities();

            desiredCapabilites.SetCapability("build", "1.0");

            //desiredCapabilites.SetCapability("browser_api_name", "Opera27");
            //desiredCapabilites.SetCapability("os_api_name", "Win8.1");


            //desiredCapabilites.SetCapability("browser_api_name", "FF35");
            //desiredCapabilites.SetCapability("os_api_name", "Win7x64-C1");


            desiredCapabilites.SetCapability("browser_api_name", GetBrowser());
            desiredCapabilites.SetCapability("os_api_name", GetPlatform());
            
            desiredCapabilites.SetCapability("screen_resolution", "1024x768");
            desiredCapabilites.SetCapability("record_video", "true");
            desiredCapabilites.SetCapability("record_network", "true");
            desiredCapabilites.SetCapability("record_snapshot", "false");

            desiredCapabilites.SetCapability("username", Username);
            desiredCapabilites.SetCapability("password", Password);
            desiredCapabilites.SetCapability("name", TestName.Prettify(TestContext.CurrentContext.Test.Name, platform, browser, version));

            // start a new remote web driver session
            var driver = new RemoteWebDriver(commandExecutorUri, desiredCapabilites);
            //driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(30));
            //driver.Manage().Timeouts().SetPageLoadTimeout(TimeSpan.FromSeconds(30));
            return driver;
        }
        private string GetPlatform()
        {
            switch (platform)
            {
                case Platform.Windows7:
                    return "Win7-C2";
                case Platform.Windows8:
                    return "Win8";
                case Platform.MacOSX10_9:
                    return "Mac10.9";
            }
            throw new Exception("Unknown plattform " + platform);
        }

        private string GetBrowser()
        {
            string browserString;
            switch (browser)
            {
                case Browser.Chrome:
                    browserString = "Chrome";
                    break;
                case Browser.Firefox:
                    browserString = "FF";
                    break;
                case Browser.InternetExplorer:
                    return "IE";
                    break;
                case Browser.Opera:
                    return "Opera";
                    break;
                case Browser.Safari:
                    return "Safari";
                    break;
                default:
                    throw new Exception("Unknown Browser " + browser);
            }
            return browserString + version.ToString().Substring(1);
        }
    }
}
﻿namespace Travel.SystemTests.Prototype.Testrunner
{
    using OpenQA.Selenium;
    using OpenQA.Selenium.Chrome;
    using OpenQA.Selenium.IE;

    using Travel.SystemTests.Prototype.Bootstrap;

    using Platform = Travel.SystemTests.Prototype.Bootstrap.Platform;

    public class LocalTestRunner : ITestRunner
    {
        public LocalTestRunner(Browser browser, Version version, Platform platform)
        {
        }

        public IWebDriver GetDriver()
        {
            return new InternetExplorerDriver();
            //return new ChromeDriver();
        }
    }
}